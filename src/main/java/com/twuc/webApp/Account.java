package com.twuc.webApp;

import javax.persistence.*;

@Entity
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL,mappedBy = "account")
    private Profile profile;

    public Long getId() {
        return id;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {

        this.profile=profile;
//        if(this.profile!=null && this.profile.equals(profile)) {
//            return;
//        }
//        this.profile = profile;
//        this.profile.setAccount(this);
    }
}

