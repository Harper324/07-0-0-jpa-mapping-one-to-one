package com.twuc.webApp;

import javax.persistence.*;

@Entity
public class Profile{


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    private Account account;

    public Long getId() {
        return id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account=account;
//        if(this.account!=null && this.account.equals(account)) {
//            return;
//        }
//        this.account = account;
//        account.setProfile(this);
    }
}
