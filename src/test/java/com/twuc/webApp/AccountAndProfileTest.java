package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class AccountAndProfileTest {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private ProfileRepository profileRepository;

    @Autowired
    EntityManager entityManager;

    public void flushAndClear(Runnable run) {
        run.run();
        entityManager.flush();
        entityManager.clear();
    }


    /*
    profile控制关系，account控制级联
    save profile then account will be saved,
    but save account profile will not be saved
    */
    @Test
    void should_not_save_account_when_save_profile() {
        flushAndClear(() -> {
            Account account = new Account();
            Profile profile = new Profile();

//            会报错
//            profile.setAccount(account);
//            不会报错
            account.setProfile(profile);
            accountRepository.save(account);
        });
        assertEquals(1L, profileRepository.findAll().get(0).getId().longValue());
    }

    @Test
    void should_save_account_when_save_profile() {
        flushAndClear(() -> {
            Account account = new Account();
            Profile profile = new Profile();
            profile.setAccount(account);

            account.setProfile(profile);
//            会报错
            profileRepository.save(profile);

        });
        assertEquals(1L, accountRepository.findAll().get(0).getId().longValue());
    }

    @Test
    void should_delete_profile_when_delete_account() {
        flushAndClear(() -> {
            Account account = new Account();
            Profile profile = new Profile();

            account.setProfile(profile);

            accountRepository.save(account);
//            error（account and profile both will be deleted
            accountRepository.deleteById(account.getId());
//            right account and profile will not be deleted
            profileRepository.deleteById(profile.getId());

        });
        assertEquals(0, profileRepository.findAll().size());
//        assertEquals(0, accountRepository.findAll().size());
    }
}
